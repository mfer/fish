FROM alpine:3.7

LABEL maintainer manah@ufmg.br

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            rsync \
            openssh \
 && rm -rf /var/cache/apk/*

RUN ssh-keygen -A

RUN adduser -D capg-cipo

RUN passwd -u capg-cipo

CMD ["/usr/sbin/sshd", "-D"]
